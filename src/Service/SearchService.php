<?php

namespace Skoromnui\Filters\Service;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Skoromnui\Filters\Filter;
use Skoromnui\Filters\SearchableModel;

class SearchService
{
    private $filtersNamespace;

    public function buildQuery(Request $filters, SearchableModel $model)
    {
        $this->filtersNamespace = $model->getFiltersNamespace();

        $query = $this->applyFilters($filters, ($model)->newQuery());

        return $query;
    }

    private function applyFilters(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            if(!empty($value)) {
                if ($filter = $this->createFilter($filterName)) {
                    $query = $this->checkAndApplyFilter($query, $filter, $value);
                }
            }
        }

        return $query;
    }

    private  function createFilter($name)
    {
        $filterClass = '\\' . $this->filtersNamespace . '\\' . Str::studly($name);

        if (class_exists($filterClass)) {
            return new $filterClass;
        }
    }

    private function checkAndApplyFilter(Builder $query, object $filter, $value)
    {
        if (!$filter instanceof Filter) {
            throw new \Exception(get_class($filter) . ' should implement interface of ' . Filter::class);
        }

        return $filter->apply($query, $value);
    }
}