<?php

namespace Skoromnui\Filters;

interface SearchableModel
{
    public function getFiltersNamespace() :string;
}