<?php

namespace Skoromnui\Filters;

use Illuminate\Database\Eloquent\Builder;

interface Filter
{
    /**
     * @param Builder $builder
     * @param mixed $value
     *
     * @return Builder $builder
     */
    public function apply(Builder $builder, $value);
}